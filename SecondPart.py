import sys, os, re

class Player:
	def __init__(self,name,batted,hits):
		self.name = name
		self.batted = batted
		self.hits = hits
		self.average = 0
	def updateData(self,batted,hits):
		self.batted += batted
		self.hits += hits

regex = re.compile(r"^([\w\s]+)[\s]batted[\s](\d)[\s]times with (\d) hits and (\d) runs$")
def match(test):
	result = regex.match(test)
	if result is not None:
		return (result.group(1),int(result.group(2)),int(result.group(3)))
	else:
		return False

if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])

filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

f = open(filename)
playerDict = {}
for line in f:
	result = match(line)
	if result :
		if result[0] in playerDict:
			playerDict[result[0]].updateData(result[1],result[2])
		else:
			player = Player(result[0],result[1],result[2])
			playerDict[result[0]] = player

playerlist = []
for player in playerDict:
	playerDict[player].average = '%.3f'%(playerDict[player].hits/float(playerDict[player].batted))
	playerlist.append(playerDict[player])
final = sorted(playerlist,key = lambda player:player.average, reverse=True)
for each in final:
	print each.name,": ",each.average

